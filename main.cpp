#include "perfect_shuffle_permutation.hpp"

#include <iostream>
#include <chrono>
#include <limits>
#include <algorithm>
#include <random>
#include <vector>

#include <cstdlib>
#include <cassert>
#include <cstdint>

int main()
{
    using T = std::intptr_t;
    using V = std::vector< T >;
    using Iterator = typename V::iterator;
    using I = typename V::difference_type;
    perfect_shuffle_permutation< Iterator > const psp_;
    try {
        std::random_device random_;
        std::mt19937_64 mersenne_(random_());
        for (I i = 2; i < std::numeric_limits< I >::max() / 4; i += 2 * std::uniform_int_distribution< I >(1, i / 2)(mersenne_)) {
            V v;
            v.reserve(i);
            for (I j = 0; j < i; ++j) {
                v.push_back(j);
            }
            using std::chrono::duration_cast;
            using std::chrono::microseconds;
            using std::chrono::steady_clock;
            {
                steady_clock::time_point const start = steady_clock::now();
                {
                    psp_.forward(v.begin(), v.end());
                }
                steady_clock::time_point const end = steady_clock::now();
                std::cout << i << ' ' << duration_cast< microseconds >(end - start).count();
            }
            {
                I const half_ = i / 2;
                for (I j = 0; j < half_; ++j) {
                    assert(v[j] == 2 * j);
                    assert(v[j + half_] == 2 * j + 1);
                }
            }
            {
                steady_clock::time_point const start = steady_clock::now();
                {
                    psp_.backward(v.begin(), v.end());
                }
                steady_clock::time_point const end = steady_clock::now();
                std::cout << ' ' << duration_cast< microseconds >(end - start).count() << std::endl;
            }
            {
                for (I j = 0; j < i; ++j) {
                    assert(v[j] == j);
                }
            }
        }
    } catch (std::bad_alloc const & ba) {
        std::cerr << ba.what() << std::endl;
    }
    return EXIT_SUCCESS;
}
