CONFIG *= console
CONFIG -= qt

TEMPLATE = app
TARGET = perfect_shuffle_permutation

QMAKE_CXXFLAGS_CXX11 = -std=gnu++1y
CONFIG *= c++11

QMAKE_CXXFLAGS_DEBUG  = -Og -ggdb3 -gdwarf-4 -march=x86-64 -mtune=generic
QMAKE_CXXFLAGS_DEBUG += -fno-inline

QMAKE_CXXFLAGS_RELEASE  = -Ofast -march=native -mtune=corei7-avx
QMAKE_CXXFLAGS_RELEASE += -floop-interchange -floop-block -floop-strip-mine

CONFIG(release, debug|release) {
    DEFINES += NDEBUG=1
}

CONFIG(debug, debug|release) {
    DEFINES += _DEBUG=1 DEBUG=1
}

DEFINES += _GLIBCXX_DEBUG=1

INCLUDEPATH += .

# Input
HEADERS += \
    "perfect_shuffle_permutation.hpp"

SOURCES += \
    "main.cpp"
